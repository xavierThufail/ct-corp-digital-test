import { createStore } from 'vuex'
import axios from 'axios'

const url = 'http://demo2725834.mockable.io/api/v1/list';

const store = createStore({
  state () {
    return {
      list: [],
      loading: false,
      error: undefined,
      props: null
    }
  },
  mutations: {
    SET_LIST (state, list) {
      state.list = list
    },
    SET_LOADING (state) {
      state.loading = !state.loading
    },
    SET_ERROR(state, err) {
      state.error = err
    },
    SET_PROPS(state, props) {
      state.props = props
    }
  },
  actions: {
    getData ({ commit }) {
      commit('SET_LOADING')

      axios({
        method: 'GET',
        url
      })
        .then(({ data }) => {
          commit('SET_LIST', data.data)
        })
        .catch(err => {
          commit('SET_ERROR', err)
        })
        .finally(() => {
          commit('SET_LOADING')
        })
    }
  }
});

export default store;
